#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  5 10:37:45 2021

@author: nielsontscha
"""
import json
import subprocess
import time
from matplotlib import pyplot as plt

ref="100"
pw="2"
kp="50"
mt="2"

subprocess.call(["./balancing",ref,pw,kp,mt])

time.sleep(0.5)
with open('data.json', "r") as file:
    data= json.loads(file.read())
    
soc_bat1=[]
soc_bat2=[]
soc_bat3=[]
soc_bat4=[]
soc_bat5=[]

out_bat1=[]
out_bat2=[]
out_bat3=[]
out_bat4=[]
out_bat5=[]

for index, line in enumerate(data['data']):

    soc_bat1.append(line[0][0])
    soc_bat2.append(line[1][0])
    soc_bat3.append(line[2][0])
    soc_bat4.append(line[3][0])
    soc_bat5.append(line[4][0])
    
    out_bat1.append(line[0][1]*100)
    out_bat2.append(line[1][1]*100)
    out_bat3.append(line[2][1]*100)
    out_bat4.append(line[3][1]*100)
    out_bat5.append(line[4][1]*100)
        

        
yticks=[0,10,20,30,40,50,60,70,80,90,100]
figure= plt.figure(dpi=1200)
ax1=plt.subplot(211, title=("Battery SOC Values ref % =",ref," kp=",kp))
ax1.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
ax1.set_yticks(yticks)
ax1.set_xlim(0,1500)
ax1.set_ylim(0,100)
plt.plot(soc_bat1)
plt.plot(soc_bat2)
plt.plot(soc_bat3)
plt.plot(soc_bat4)
plt.plot(soc_bat5)

ax2=plt.subplot(212, title="PWM Ref. Output Values %")
ax2.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
ax2.set_yticks(yticks)
ax2.set_xlim(0,1500)
ax2.set_ylim(0,100)
plt.plot(out_bat1)
plt.plot(out_bat2)
plt.plot(out_bat3)
plt.plot(out_bat4)
plt.plot(out_bat5)
figure.tight_layout()


        
        
