#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
float kp;
float ki;
float cycle_time;
float limit_up;
float limit_down;
float ext_in;
float p_out;
float i_out;
float output;
float anti_windup_lim;
float ref_value;
float soc;
} pi_controller;

float init_control(pi_controller pi, float kp_init, float ki_init,float init_cycle_time,float init_limit_up,float init_limit_down, float init_anti_windup_lim)
{
    pi.kp=kp_init;
    pi.ki=ki_init;
    pi.cycle_time=init_cycle_time;
    pi.limit_up=init_limit_up;
    pi.limit_down=init_limit_down;
    pi_anti_windup_lim=init_anti_windup_lim;

}

float set_kp(pi_controller pi,float new_kp)
{
    pi.kp=new_kp;
}

float set_ki(pi_controller pi,float new_ki)
{
    pi.ki=new_ki;
}

float set_soc(pi_controller pi,float new_soc)
{
    pi.soc=new_soc;
}
float set_ext_in(pi_controller pi,float new_ext_int)
{
    pi.ext_in=new_ext_in;
}


float run_cycle(pi_controller pi, float actual_value, float ref_value){
    pi.p_out = (actual_value-ref_value)*kp;
    pi.i_out +=(actual_value-ref_value)*ki;
    if (pi.i_out>=pi.anti_windup_lim)
    {
        pi.i_out=pi.anti_windup_lim;
    }
    
    pi.output=pi.i_out+pi.p_out+pi.ext_in;
    if(pi.output>limit_up)
    {
        pi.output=limit_up;
    
    }
    if(pi.output<limit_down)
    {
        pi.output=limit_down;

    }
    if (pi.soc<25)
    {
        pi.output=limit_down;
    }
    

}

float soma(float values[])
{
	int i=0;
	float sum =0;
	for (i = 0; i < 5; i++)
	{
		sum=sum+values[i];
	}
	return sum;
}
float average(float values[])
{
	int i=0;
	float average=0;
	average=soma(values)/5;
	return average;
}

float deviation(float values[])
{
	int i=0;
	float avg=average(values),deviation=0;
	for (i = 0; i < 5; i++)
	{
		deviation=deviation+(fabs(values[i]-avg));
	}
	deviation=deviation/5;
	return deviation;
}

float power(float base, int exponent)
{
	int result=1;
	for (; exponent>0; exponent--)
		{
		result = result * base;
		}
	return result;
}


float * run_controllers(float soc_vec[],float ref_val, float kp, float ki, float ext_in, pi_controller [])
{
                static float normval[5]={0,0,0,0,0};
                
                for (i = 0; i < 5; i++)
                {
                pi[i].
				diff=soc_vec[i]-average(soc_vec);
				if (diff>=10)
				{
					
					set_kp(pi[i],kp);
					set_ki(pi[i],ki);

				}
				else if	(diff >=0)			
				{
					set_kp(pi[i],(diff/10)*kp);
					set_ki(pi[i],(diff/10)*ki);

				}
				else
				{
					set_kp(pi[i],1);
					set_ki(pi[i],1);
				}
				run_cycle(pi[i],ref_val)
                normval[i]=pi[i].output;
				}


}
 int main (int argc, char *argv[])
{	

	printf("number of arguments: %d\n",argc);
	int l;
	printf("\nexe name=%s", argv[0]);

 	for (l=1; l< argc; l++) {
	printf("\narg%d=%s\n", l, argv[l]);
 	}
	FILE *fp;
	fp = fopen("./data.json", "w+");
	float ref=atof(argv[1]);
	int norm_power=atoi(argv[2]);
	float kp=atof(argv[3]);
	float ki=atof(argv[4]);
	float val[5]={40.0,52.0,61.0,70.0,45.0};
	float nom_volt=12; //12V nominal voltage
	float res =0.001;//resistance in ohms
	float *out_val;
	int i=0;
    pi_controller batt_control[5];
    pi_controller ref_control;



// MAKE NESTED LOOP, THE INPUT FOR THE INDIVIDUAL BATTERY 
// WILL BE THE BATTERY 
  // float kp_init, float ki_init,float init_cycle_time,
  //float init_limit_up,float init_limit_down,
  // float init_anti_windup_lim)
    init_control(ref_control,100,10,0.01,1,0,0.3);
    for (i = 0; i < 5; i++)
	{
		init_control(batt_control[i],100,10,0.01,1,0,0.3);
	}
	
	//below here a small simulation
	
	float batt_cap = 300.0;// battery capacity in mAH
	float coulumbs_batt=batt_cap/1000*3600; //total coulumbs in the battery
	float time=60; //time in seconds
	float curr[5]={0,0,0,0,0};//vector to store the currents
	float consum=0;//couloumbs consumption of the battery
	int j=0;
	float dev_med=100;
	float curr_average=0;
	fprintf(fp,"{\"data\":[");
	while (1)
	{
		
		
    	
		//out_val=balance(val,norm _power,ref,method,kp);
		//out_val=
		for (i = 0; i < 5; i++)
		{	

			curr[i]=(*(out_val+i))*nom_volt/res/100;
			consum=curr[i]*time;
			/*if (i==1)
			{
				printf("output %d normalized to 100=%.2f\n",i,(*(out_val+i)));
				printf("soc: %f, average soc: %f\n",val[i],average(val));
			}*/
			//printf("coulumbs consumed battery %d in %.1f seconds were %.2f\n",i,time,consum);
			val[i]=val[i]-(consum/coulumbs_batt)/100;
		//	printf("delta soc is= %f\n",(consum/coulumbs_batt)/100);
		//	printf("new SOC for batt %d is: %.2f\n",i,val[i]);	
		
		//json encoding printing

			if (i==0)
			{
				fprintf(fp,"[[%.2f,%.3f]",val[i],*(out_val+i));
			}
			else if(i==4)
			{
				fprintf(fp,",[%.2f,%.3f]]",val[i],*(out_val+i));
			}
			else
			{
				fprintf(fp,",[%.2f,%.3f]",val[i],*(out_val+i));

			}
		}	
		//printf("j=%d\n",j);
			j=j+1;
			curr_average+=average(curr);
			dev_med=deviation(val);
			
			if (j>2000|| dev_med<0.5)
			{
				break;
			}
			else
			{
				fprintf(fp,",");
			}
		
	}
	
	fprintf(fp,"]}");
	fclose(fp);
	
	//printf("\nthe time required for balancing was %.2f hours\n",j*time/3600);
	//printf("average current of the batteries was %.2f Ampere\n",curr_average/j);
	for (i=0;i<5; i++)
	{
		printf("final SOC for batt %d is: %.2f\n",i,val[i]);
		
			
	}

	
		printf("time required to achieve balancing=%.0f\n minutes\n",(float)j);
	return 0;
}
